package io.morgaroth.parcelsscapper.scrapping.olx

import java.time.LocalDate

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OlxOfferScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {

  val underTest = new OlxOfferScrapper()

  behavior of "OlxOfferScrapper"

  it should "parse offer page" in {
    underTest.parseOffer(loadRes("olx/olx_offer_page_1.html")).copy(description = "") shouldBe
      OlxFullOffer(Some("609704468,791731861"), "Działka z możliwością zalesienia Rekreacyjna z Dojazdem 14,3 a", "", LocalDate.of(2020, 5, 5), Some(10000))
  }

  it should "parse offer page 2" in {
    underTest.parseOffer(loadRes("olx/olx_offer_page_3.html")).copy(description = "") shouldBe
      OlxFullOffer(None, "dzialka", "", LocalDate.of(2020, 5, 12), Some(22980))
  }
}
