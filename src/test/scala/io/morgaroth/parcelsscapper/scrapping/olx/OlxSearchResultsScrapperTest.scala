package io.morgaroth.parcelsscapper.scrapping.olx

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.jsoup.Jsoup
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OlxSearchResultsScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {
  val underTest = new OlxSearchResultsScrapper()

  behavior of "OlxSearchResultsScrapper"

  it should "parse results list correctly" in {
    val results = underTest.parseResultsFrom(loadRes("olx/olx_search_result_1.html"))
    results should have size 42
  }

  it should "parse offer item well" in {
    val result = underTest.parseOfferItem(Jsoup.parseBodyFragment(loadRes("olx/olx_item_1.html")))
    result shouldBe OlxSearchEntry("601968513", 43000, "Korzenna", "https://www.olx.pl/oferta/dzialka-budowlana-15-ar-CID3-IDEJNwz.html")
  }

  it should "parse pager well on the first page" in {
    val result = underTest.parsePager(loadRes("olx/olx_search_result_1.html"))
    result shouldBe OlxSearchPager(26, 1, None, Some("https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/malopolskie/?search%5Bfilter_float_m%3Afrom%5D=1500&page=2"))
  }

  it should "parse pager well on a middle page" in {
    val result = underTest.parsePager(loadRes("olx/olx_search_result_middle_1.html"))
    result shouldBe OlxSearchPager(26, 9, Some("https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/malopolskie/?search%5Bfilter_float_m%3Afrom%5D=1500&page=8"), Some("https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/malopolskie/?search%5Bfilter_float_m%3Afrom%5D=1500&page=10"))
  }

  it should "parse pager well on the last page" in {
    val result = underTest.parsePager(loadRes("olx/olx_search_result_last_1.html"))
    result shouldBe OlxSearchPager(26, 26, Some("https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/malopolskie/?search%5Bfilter_float_m%3Afrom%5D=1500&page=25"), None)
  }

}
