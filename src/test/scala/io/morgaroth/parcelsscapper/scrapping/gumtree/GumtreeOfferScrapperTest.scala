package io.morgaroth.parcelsscapper.scrapping.gumtree

import java.time.LocalDate

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class GumtreeOfferScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {

  val underTest = new GumtreeOfferScrapper()

  behavior of "GumtreeOfferScrapper"

  it should "parse offer correctly" in {
    underTest.parseOffer(loadRes("gumtree/single_offer_1.html")).copy(description = "") shouldBe GumtreeFullOffer("1007269025370911543290009", Some("505105224"), "Wieliczka, Małopolskie", Some("Atrakcyjna działka w Grodkowicach."), "", LocalDate.of(2020, 5, 7), dead = false)
    underTest.parseOffer(loadRes("gumtree/single_offer_2.html")).copy(description = "") shouldBe GumtreeFullOffer("1004390676830912099700009", Some("505975843"), "Wieliczka, Małopolskie", Some("działka budowlana 30a w Raciborsku koło Wieliczki"), "", LocalDate.of(2019, 2, 27), dead = false)
  }

}