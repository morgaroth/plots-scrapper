package io.morgaroth.parcelsscapper.scrapping.morizon

import java.time.LocalDate

import io.morgaroth.parcelsscapper.testhelpers.ResourcesLoader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MorizonOfferScrapperTest extends AnyFlatSpec with Matchers with ResourcesLoader {

  val underTest = new MorizonOfferScrapper()

  behavior of "MorizonOfferScrapper"

  it should "parse offer page" in {
    underTest.unsafeParseOffer(loadRes("morizon/morizon_offer_page_1.html")).copy(description = "") shouldBe
      MorizonFullOffer("1mzn2036235313", "+48886809991", "Grodkowice, Wiejska 121903_2.0", "", 1500, LocalDate.of(2020, 5, 13), LocalDate.of(2020,5,13))
  }
}
