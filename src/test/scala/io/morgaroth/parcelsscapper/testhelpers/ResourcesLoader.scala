package io.morgaroth.parcelsscapper.testhelpers

import scala.io.Source

trait ResourcesLoader {

  def loadRes(path: String): String = Source.fromResource(path).mkString

}
