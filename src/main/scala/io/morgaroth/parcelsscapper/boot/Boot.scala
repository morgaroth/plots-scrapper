package io.morgaroth.parcelsscapper.boot

import io.morgaroth.parcelsscapper.storage.MongoRepository

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

case class ExportRow(mapId: String, price: Long, area: Long, place: String) {
  override def toString = s"$mapId, ${price / 1000}k zł, ${area / 100} ar, ${place.trim}"
}

object Boot {
  def main(args: Array[String]): Unit = {
    val genericDb = MongoRepository.GenericOffersDB()

    val work = for {
      allGeneric <- genericDb.findAll()
      geneticLegit = allGeneric.filter(_.mapId.isDefined).map(x => ExportRow(x.mapId.get, x.price, x.area.getOrElse(0), x.place.getOrElse("Bez miejsca")))
    } yield geneticLegit.sortBy(_.mapId)

    val rows = Await.result(work, 10.seconds)

    rows.map(println)
  }
}
