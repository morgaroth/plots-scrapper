package io.morgaroth.parcelsscapper.boot

import cats.instances.future.catsStdInstancesForFuture
import cats.instances.vector._
import cats.syntax.traverse._
import io.morgaroth.parcelsscapper.storage.MongoRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Migrator {
  def main(args: Array[String]): Unit = {
    val genericDb = MongoRepository.GenericOffersDB()

    val work = genericDb.findAll().flatMap(_.traverse { entry =>
      //      if (entry.link.endsWith(";promoted")) {
      //        genericDb.updateLink(entry._id, entry.link.stripSuffix(";promoted"))
      //      } else
      Future.successful(1)
    })

    Await.result(work, 5.minutes)
  }
}
