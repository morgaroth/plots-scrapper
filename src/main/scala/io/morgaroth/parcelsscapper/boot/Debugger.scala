package io.morgaroth.parcelsscapper.boot

import java.util.UUID

import io.morgaroth.parcelsscapper.common.{PlaceAI, RichClasses}
import io.morgaroth.parcelsscapper.storage.MongoRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Debugger extends RichClasses {
  def main(args: Array[String]): Unit = {
    val genericDb = MongoRepository.GenericOffersDB()

    val id = UUID.fromString("")
    val entry = genericDb.getById(id).futureValue(10.seconds).fold(throw _, identity)

  }
}
