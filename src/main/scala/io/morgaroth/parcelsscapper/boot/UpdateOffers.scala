package io.morgaroth.parcelsscapper.boot

import cats.instances.future.catsStdInstancesForFuture
import cats.instances.vector._
import cats.syntax.traverse._
import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.parcelsscapper.common.{Gumtree, NonActualOffer, OfferParsed, Olx, Otodom, Morizon}
import io.morgaroth.parcelsscapper.scrapping.gumtree.{GumtreeOfferScrapper, GumtreeSearchResultsScrapper}
import io.morgaroth.parcelsscapper.scrapping.morizon.{MorizonOfferScrapper, MorizonSearchResultsScrapper}
import io.morgaroth.parcelsscapper.scrapping.olx.{OlxOfferScrapper, OlxSearchResultsScrapper}
import io.morgaroth.parcelsscapper.scrapping.otodom.{OtodomOfferScrapper, OtodomSearchResultsScrapper}
import io.morgaroth.parcelsscapper.storage.{MongoRepository, ParcelOfferDbEntry}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object UpdateOffers extends LazyLogging {
  def main(args: Array[String]): Unit = {
    val genericDb = MongoRepository.GenericOffersDB("mongodb://localhost/parcels")

    val olxScrapper = new OlxSearchResultsScrapper()
    val olxOfferScrapper = new OlxOfferScrapper()
    val olxResults = Vector(
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/bochnia/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa&search%5Bfilter_float_m%3Afrom%5D=1000",
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/wieliczka/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa&search%5Bfilter_float_m%3Afrom%5D=1000",
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/myslenice/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa&search%5Bfilter_float_m%3Afrom%5D=1000",

      // Sułów
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/sulow_107167/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
      // Biskupice
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/biskupice_42315/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
       // Sułków
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/sulkow_37533/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
      // Łazany
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/lazany_100917/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
      // Nowy wiśnicz
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/nowy-wisnicz/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
      // leksandrowa
      "https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/leksandrowa/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_enum_type%5D%5B1%5D=dzialki-inwestycyjne&search%5Bfilter_enum_type%5D%5B2%5D=dzialki-rolno-budowlane&search%5Bfilter_enum_type%5D%5B3%5D=dzialka-siedliskowa",
    ).flatMap(olxScrapper.parseAllResultsFor)

    val olxWork: Future[Vector[ParcelOfferDbEntry]] = olxResults.distinctBy(_.id).filter(_.link.startsWith("https://www.olx.pl")).traverse { result =>
      genericDb.findByOfferId(result.id, Olx).flatMap {
        case None =>
          logger.info(s"new offer olx ${result.link}")
          val fullOffer = olxOfferScrapper.parseOfferUrl(result.link)
          val offer = ParcelOfferDbEntry(result, fullOffer)
          genericDb.store(offer).map(_ => List(offer))
        case Some(_) => Future.successful(Nil)
      }
    }.map(_.flatten)
    val olxNewOffers = Await.result(olxWork, 5.minutes)

    val gumtreeScrapper = new GumtreeSearchResultsScrapper()
    val gumtreeOfferScrapper = new GumtreeOfferScrapper()
    val gumtreeResults = Vector(
      "https://www.gumtree.pl/s-dzialki/wieliczka/v1c9194l3200224p1?priceType=FIXED",
      "https://www.gumtree.pl/s-dzialki/bochnia/v1c9194l3200200p1?priceType=FIXED",

      "https://www.gumtree.pl/s-dzialki/malopolskie/v1c9194l3200003p1?q=su%C5%82%C3%B3w&priceType=FIXED", // Sułów
      "https://www.gumtree.pl/s-dzialki/malopolskie/v1c9194l3200003p1?q=su%C5%82k%C3%B3w&priceType=FIXED", // Sułków
      "https://www.gumtree.pl/s-dzialki/bochnia/v1c9194l3200200p1?q=zawada&priceType=FIXED", // Zawada k. Bochni
      "https://www.gumtree.pl/s-dzialki/wieliczka/v1c9194l3200224p1?q=%C5%82azany&priceType=FIXED", // Łazany
      "https://www.gumtree.pl/s-dzialki/malopolskie/v1c9194l3200003p1?q=niegowi%C4%87&priceType=FIXED", // Niegowić
      "https://www.gumtree.pl/s-dzialki/bochnia/v1c9194l3200200p1?q=wi%C5%9Bnicz&priceType=FIXED", // Wiśnicze
    ).flatMap(gumtreeScrapper.parseAllResultsFor)

    val gumtreeWork = gumtreeResults.distinctBy(_.id).traverse { result =>
      genericDb.findByOfferId(result.id, Gumtree).flatMap {
        case None =>
          logger.info(s"new offer gumtree ${result.link}")
          val fullOffer = gumtreeOfferScrapper.parseOfferUrl(result.link)
          val entry = ParcelOfferDbEntry(result, fullOffer)
          genericDb.store(entry).map(_ => List(entry))
        case Some(_) => Future.successful(Nil)
      }
    }.map(_.flatten)
    val gumtreeNewOffers = Await.result(gumtreeWork, 5.minutes)

    val otodomScrapper = new OtodomSearchResultsScrapper()
    val otodomOfferScrapper = new OtodomOfferScrapper()

    val otodomResults = Vector(
      "https://www.otodom.pl/sprzedaz/dzialka/powiat-wielicki/?search%5Bfilter_float_m%3Afrom%5D=1000&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=163",
      "https://www.otodom.pl/sprzedaz/dzialka/mogilany/?search%5Bfilter_float_m%3Afrom%5D=1000&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=407&search%5Bcity_id%5D=100",
      "https://www.otodom.pl/sprzedaz/dzialka/radziszow/?search%5Bfilter_float_m%3Afrom%5D=1000&search%5Bcity_id%5D=8181",
      "https://www.otodom.pl/sprzedaz/dzialka/szarow/?search%5Bfilter_float_m%3Afrom%5D=1000&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=163&search%5Bcity_id%5D=13693",
      "https://www.otodom.pl/sprzedaz/dzialka/powiat-bochenski/?search%5Bfilter_float_m%3Afrom%5D=1000&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=145",

      "https://www.otodom.pl/sprzedaz/dzialka/lazany/?search%5Bfilter_float_m%3Afrom%5D=1300&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=163&search%5Bcity_id%5D=12508",
      "https://www.otodom.pl/sprzedaz/dzialka/siepraw/?search%5Bfilter_float_m%3Afrom%5D=1300&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=153&search%5Bcity_id%5D=473",
      "https://www.otodom.pl/sprzedaz/dzialka/borzeta/?search%5Bfilter_float_m%3Afrom%5D=1300&search%5Bregion_id%5D=6&search%5Bsubregion_id%5D=153&search%5Bcity_id%5D=7004",
      // wiśnicze
      "https://www.otodom.pl/sprzedaz/dzialka/?search%5Bfilter_float_m%3Afrom%5D=1300&locations%5B0%5D%5Bregion_id%5D=6&locations%5B0%5D%5Bsubregion_id%5D=145&locations%5B0%5D%5Bcity_id%5D=14180&locations%5B1%5D%5Bregion_id%5D=6&locations%5B1%5D%5Bsubregion_id%5D=145&locations%5B1%5D%5Bcity_id%5D=416&locations%5B2%5D%5Bregion_id%5D=6&locations%5B2%5D%5Bsubregion_id%5D=145&locations%5B2%5D%5Bcity_id%5D=23925",
    ).flatMap(otodomScrapper.parseAllResultsFor).distinctBy(_.id)

    val otodomWork = otodomResults.distinctBy(_.id).traverse { result =>
      genericDb.findByOfferId(result.id, Otodom).flatMap {
        case None =>
          otodomOfferScrapper.parseOfferUrl(result.link) match {
            case NonActualOffer() => Future.successful(Nil)
            case OfferParsed(fullOffer) =>
              logger.info(s"new offer otodom ${result.link}")
              val entry = ParcelOfferDbEntry(result, fullOffer)
              genericDb.store(entry).map(_ => List(entry))
          }
        case Some(_) => Future.successful(Nil)
      }
    }.map(_.flatten)
    val otodomNewOffers = Await.result(otodomWork, 5.minutes)


    val morizonScrapper = new MorizonSearchResultsScrapper()
    val morizonOfferScrapper = new MorizonOfferScrapper()

    val morizonResults = Vector(
      "https://www.morizon.pl/dzialki/wielicki/?ps%5Bliving_area_from%5D=1400",
      "https://www.morizon.pl/dzialki/biskupice/lazany/?ps%5Bliving_area_from%5D=1000", // łazany
      "https://www.morizon.pl/dzialki/bochenski/nowy-wisnicz/?ps%5Bliving_area_from%5D=1000", // Nowy Wiśnicz
      "https://www.morizon.pl/dzialki/bochnia/lapczyca/?ps%5Bliving_area_from%5D=1000", // Łapczyca
      "https://www.morizon.pl/dzialki/bochnia/zawada/?ps%5Bliving_area_from%5D=1000", // Zawada
    ).flatMap(morizonScrapper.parseAllResultsFor).distinctBy(_.id)

    val morizonWork = morizonResults.distinctBy(_.id).traverse { result =>
      genericDb.findByOfferId(result.id, Morizon).flatMap {
        case None =>
          val fullOffer = morizonOfferScrapper.parseOfferUrl(result.link)
          logger.info(s"new offer morizon ${result.link}")
          val entry = ParcelOfferDbEntry(result, fullOffer)
          genericDb.store(entry).map(_ => List(entry))
        case Some(_) => Future.successful(Nil)
      }
    }.map(_.flatten)
    val morizonNewOffers = Await.result(morizonWork, 5.minutes)

    println(s"There were ${morizonNewOffers.length} new offers in Morizon")
    println(s"There were ${otodomNewOffers.length} new offers in Otodom")
    println(s"There were ${gumtreeNewOffers.length} new offers in Gumtree")
    println(s"There were ${olxNewOffers.length} new offers in Olx")

  }
}
