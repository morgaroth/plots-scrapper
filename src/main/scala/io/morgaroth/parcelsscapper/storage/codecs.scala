package io.morgaroth.parcelsscapper.storage

import java.time.{Instant, ZoneOffset, ZonedDateTime}

import io.morgaroth.parcelsscapper.common.{OfferRate, OfferSource}
import org.bson.codecs.configuration.{CodecConfigurationException, CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonType, BsonWriter}
import org.mongodb.scala.model.geojson.{Point, Position}

object OfferRateCodecP extends CodecProvider {
  override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
    if (OfferRate.valuesClasses.contains(clazz)) OfferRateCodec.asInstanceOf[Codec[T]] else null
  }
}
object OfferSourceCodecP extends CodecProvider {
  override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
    if (OfferSource.valuesClasses.contains(clazz)) OfferSourceCodec.asInstanceOf[Codec[T]] else null
  }
}


object OfferRateCodec extends Codec[OfferRate] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext) = {
    reader.getCurrentBsonType match {
      case BsonType.STRING =>
        OfferRate.byName.apply(reader.readString())
      case t =>
        throw new CodecConfigurationException(s"Could not decode OfferRate, expected STRING BsonType but got '$t'.")
    }
  }

  override def encode(writer: BsonWriter, value: OfferRate, encoderContext: EncoderContext): Unit = {
    writer.writeString(value.name)
  }

  override def getEncoderClass = classOf[OfferRate]
}

object OfferSourceCodec extends Codec[OfferSource] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext): OfferSource = {
    reader.getCurrentBsonType match {
      case BsonType.STRING =>
        OfferSource.byName.apply(reader.readString())
      case t =>
        throw new CodecConfigurationException(s"Could not decode OfferRate, expected STRING BsonType but got '$t'.")
    }
  }

  override def encode(writer: BsonWriter, value: OfferSource, encoderContext: EncoderContext) = {
    writer.writeString(value.name)
  }

  override def getEncoderClass: Class[OfferSource] = classOf[OfferSource]
}

object UTCZonedDateTimeMongoCodec extends Codec[ZonedDateTime] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext): ZonedDateTime = {
    reader.getCurrentBsonType match {
      case BsonType.DATE_TIME =>
        Instant.ofEpochMilli(reader.readDateTime).atZone(ZoneOffset.UTC)
      case t =>
        throw new CodecConfigurationException(s"Could not decode into ZonedDateTime, expected DATE_TIME BsonType but got '$t'.")
    }
  }

  override def encode(writer: BsonWriter, value: ZonedDateTime, encoderContext: EncoderContext): Unit = {
    try {
      writer.writeDateTime(value.withZoneSameInstant(ZoneOffset.UTC).toInstant.toEpochMilli)
    } catch {
      case e: ArithmeticException =>
        throw new CodecConfigurationException(s"Unsupported LocalDateTime value '$value' could not be converted to milliseconds: ${e.getMessage}", e)
    }
  }

  override def getEncoderClass: Class[ZonedDateTime] = classOf[ZonedDateTime]
}

case class LocationCord(x: Double, y: Double) {
  override def toString = s"$x,$y"

  def toGoogleMapsLink = s"https://www.google.com/maps?t=k&q=loc:$x+$y"
}

class LocationCordCodecProvider extends CodecProvider {
  override def get[T](clazz: Class[T], registry: CodecRegistry) = {
    if (clazz eq classOf[LocationCord]) {
      val pointCodec = registry.get(classOf[Point])
      new Codec[LocationCord] {
        override def decode(reader: BsonReader, decoderContext: DecoderContext) = {
          val p = pointCodec.decode(reader, decoderContext)
          LocationCord(p.getPosition.getValues.get(0), p.getPosition.getValues.get(1))
        }

        override def encode(writer: BsonWriter, value: LocationCord, encoderContext: EncoderContext) = {
          pointCodec.encode(writer, Point(Position(value.x, value.y)), encoderContext)
        }

        override def getEncoderClass: Class[LocationCord] = classOf[LocationCord]
      }.asInstanceOf[Codec[T]]
    } else null
  }
}
