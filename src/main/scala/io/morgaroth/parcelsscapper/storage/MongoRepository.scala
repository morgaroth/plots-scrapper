package io.morgaroth.parcelsscapper.storage

import java.util.UUID

import com.mongodb.ConnectionString
import io.morgaroth.parcelsscapper.scrapping.gumtree.{GumtreeFullOffer, GumtreeSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.morizon.{MorizonFullOffer, MorizonSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.olx.{OlxFullOffer, OlxSearchEntry}
import io.morgaroth.parcelsscapper.scrapping.otodom.{OtodomFullOffer, OtodomSearchEntry}
import org.bson.UuidRepresentation
import org.bson.codecs.Codec
import org.bson.codecs.configuration.{CodecProvider, CodecRegistries}
import org.mongodb.scala.bson.codecs.{DocumentCodecProvider, IterableCodecProvider, Macros}
import org.mongodb.scala.{MongoClient, MongoClientSettings}

import scala.reflect.ClassTag

object MongoRepository {
  private val require = Seq(classOf[Codec[_]], classOf[DocumentCodecProvider], classOf[IterableCodecProvider])

  val customCodecProviders = CodecRegistries.fromProviders(new LocationCordCodecProvider, OfferRateCodecP, OfferSourceCodecP)
  val customCodecs = CodecRegistries.fromCodecs(UTCZonedDateTimeMongoCodec, OfferRateCodec, OfferSourceCodec)
  val initialCodecs = CodecRegistries.fromRegistries(MongoClient.DEFAULT_CODEC_REGISTRY, customCodecProviders, customCodecs)

  private val OlxSearchResultCodec = CodecRegistries.fromCodecs(Macros.createCodec[OlxSearchEntry])
  private val OlxFullOfferCodec = CodecRegistries.fromCodecs(Macros.createCodec[OlxFullOffer](initialCodecs))
  private val GumtreeSearchResultCodec = CodecRegistries.fromCodecs(Macros.createCodec[GumtreeSearchEntry])
  private val GumtreeFullOfferCodec = CodecRegistries.fromCodecs(Macros.createCodec[GumtreeFullOffer](initialCodecs))
  private val OtodomSearchResultCodec = CodecRegistries.fromCodecs(Macros.createCodec[OtodomSearchEntry])
  private val OtodomFullOfferCodec = CodecRegistries.fromCodecs(Macros.createCodec[OtodomFullOffer](initialCodecs))
  private val MorizonSearchEntryCodec = CodecRegistries.fromCodecs(Macros.createCodec[MorizonSearchEntry])
  private val MorizonFullOfferCodec = CodecRegistries.fromCodecs(Macros.createCodec[MorizonFullOffer](initialCodecs))
  private val ParcelOfferDbEntryCodec = Macros.createCodecProviderIgnoreNone[ParcelOfferDbEntry]

  private def createCollection[K, V: ClassTag](uri: String, collection: String, codecs: CodecProvider) = {
    val finalCodec = CodecRegistries.fromRegistries(
      initialCodecs,
      CodecRegistries.fromProviders(codecs),
      OlxSearchResultCodec, OlxFullOfferCodec,
      GumtreeSearchResultCodec, GumtreeFullOfferCodec,
      OtodomSearchResultCodec, OtodomFullOfferCodec,
      MorizonSearchEntryCodec, MorizonFullOfferCodec,
    )

    val mongoUri = new ConnectionString(uri)
    val clientSettings = MongoClientSettings.builder().uuidRepresentation(UuidRepresentation.JAVA_LEGACY).applyConnectionString(mongoUri).codecRegistry(finalCodec).build()
    val mongoClient = MongoClient(clientSettings)
    val database = mongoClient
      .getDatabase(mongoUri.getDatabase)

    database.getCollection[V](collection)
  }

  def GenericOffersDB(uri: String = "mongodb://localhost/parcels") = {
    val db = new ParcelOffersDB(createCollection[UUID, ParcelOfferDbEntry](uri, "GenericOffers", ParcelOfferDbEntryCodec))
    db.initialize()
    db
  }
}