package io.morgaroth.parcelsscapper.common

sealed trait OfferParsing[T] extends Product with Serializable

case class OfferParsed[T](value: T) extends OfferParsing[T]

case class NonActualOffer[T]() extends OfferParsing[T]
