package io.morgaroth.parcelsscapper.common

import java.net.URLEncoder

import io.morgaroth.parcelsscapper.storage.LocationCord

object LinkGenerators {
  def mapsLinkOfPlace(placeName: String) = {
    val link = Locations.predefinedPlaces.get(placeName).map(_.toGoogleMapsLink).getOrElse(s"https://www.google.com/maps?t=k&q=${URLEncoder.encode(placeName, "utf-8")}")
    println(link)
    link
  }
}

object Locations {
  val predefinedPlaces = Map(
    "Siercza"    -> LocationCord(49.972002, 20.042796),
    "Sławkowice" -> LocationCord(49.936430, 20.145012),
    "Zagórze"    -> LocationCord(49.992258, 20.189549),
    "Gorzków"    -> LocationCord(49.926212, 20.028652),
    "Kunice"     -> LocationCord(49.900317, 20.146296),
  )
}