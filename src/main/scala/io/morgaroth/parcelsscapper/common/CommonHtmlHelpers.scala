package io.morgaroth.parcelsscapper.common

import io.morgaroth.parcelsscapper.errors.ParsingAssertion

trait CommonHtmlHelpers {
  def normalizeText(input: String): String =
    input
      .replaceAll("""\h+""", " ")
      .split("\n").map(_.trim).filter(_.nonEmpty).mkString("\n")
      .replaceAll("ą", "ą")
      .replaceAll("ć", "ć")
      .replaceAll("ę", "ę")
      .replaceAll("ń", "ń")
      .replaceAll("ó", "ó")
      .replaceAll("ś", "ś")
      .replaceAll("Ś", "ś")
      .replaceAll("ż", "ż")
      .replaceAll("Ż", "ż")
      .replaceAll("ź", "ź")
      .replaceAll("Ź", "ź")

  def normalizeTextToOneLine(input: String): String =
    normalizeText(input.replaceAll("\n", " "))

  def validateAndParseMoney(input: String): Int = {
    if (input.matches("""\d+( \d+)*(,\d{2})? zł""")) {
      input.replaceAll("""[ (zł)]+""", "").replace(",", ".").toDouble.toInt
    } else {
      raise(s"input text: '$input' is not value for money")
    }
  }

  def validateAndParseAreaInSqMeters(input: String): Int = {
    if (input.matches("""\d+( \d+)*(,\d{2})? m²""")) {
      input.replaceAll("""[ (m²)]+""", "").replace(",", ".").toDouble.toInt
    } else {
      raise(s"input text: '$input' is not value for area in m²")
    }
  }

  def parsePhoneNumbers(elements: Vector[String]): String = {
    elements.map(_.replaceAll("""[\h+ \-]""", ""))
      .filter(_ != "12633")
      .map(x => if (x.endsWith("(biuro)")) x.stripSuffix("(biuro)") else x)
      .collect {
        case validshort if validshort.matches("""\d{9}""") => s"+48$validshort"
        case validLongWithout if validLongWithout.matches("""48\d{9}""") => s"+$validLongWithout"
        case validLong if validLong.matches("""\+48\d{9}""") => validLong
        case homeWithPrefix if homeWithPrefix.matches("""\d+/\d{7}""") => "+48" + homeWithPrefix.replace("/", "")
        case homeWithPrefix if homeWithPrefix.matches("""\d+/\d{7}""") => "+48" + homeWithPrefix.replace("/", "")
        case another => raise(s"don't know how to parse $another")
      }.distinct.sorted.mkString(",")
  }

  def raise(problem: String) = {
    throw ParsingAssertion(problem)
  }

  implicit class RichString(value: String) {
    def normalize: String = normalizeText(value)

    def toOneLineNormalized: String = normalize.replaceAll("\n", " ")
  }

}

object CommonHtmlHelpers extends CommonHtmlHelpers