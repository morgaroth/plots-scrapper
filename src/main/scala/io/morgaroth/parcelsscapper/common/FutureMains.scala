package io.morgaroth.parcelsscapper.common

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

trait FutureMain extends LazyLogging {
  def tm = 1.minute

  def main(args: Array[String]): Unit = {
    val value = fmain(args.toList).map {
      case e: Either[_, _] => logger.error("FutureMain got either inside, use EitherFutureMain instead. {}", e)
    }
    Await.result(value, tm)
  }

  def fmain(args: List[String]): Future[_]

}

trait EitherFutureMain extends LazyLogging {
  def tm = 1.minute

  def main(args: Array[String]): Unit = {
    val value = fmain(args.toList).map {
      _.left.map(e => logger.error("There was an error", e))
    }
    Await.result(value, tm)
  }

  def fmain(args: List[String]): Future[Either[Throwable, _]]

}
