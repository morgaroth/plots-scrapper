package io.morgaroth.parcelsscapper.common

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.sys.process._

object Browser {
  def open(url: String): Unit = Future(s"google-chrome $url".!).failed.foreach(println(_))

}
