package io.morgaroth.parcelsscapper.scrapping.olx

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import io.morgaroth.parcelsscapper.scrapping.DebugHelpers
import org.jsoup.Jsoup

case class OlxFullOffer(phone: Option[String], title: String, description: String, addDate: LocalDate, area: Option[Long])

class OlxOfferScrapper() extends CommonHtmlHelpers with RichClasses {

  private val dateParser = DateTimeFormatter.ofPattern("'o' HH:mm, d MMMM yyyy")

  def parseOfferUrl(url: String): OlxFullOffer = {
    if (url.contains("otodom.pl")) throw new RuntimeException(s"dafuq $url")
    val doc = RJsoup.get(url).get
    try {
      parseOffer(doc)
    } catch {
      case e: NullPointerException =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName, doc, url, e)
        throw e
    }
  }

  def parseOffer(str: String): OlxFullOffer = {
    val doc = Jsoup.parse(str)
    //    val dead = Option(warn).map(_.text()).exists(_.contains("wygasło"))

    val details = doc.selectSingle("ul.offer-details")
    val area = validateAndParseAreaInSqMeters(details.selectSingle("li:contains(Powierzchnia)").selectSingle("*[class=offer-details__value]").text().normalize)
    val summary = doc.selectSingle("ul.offer-bottombar__items")
    val addDate = summary.selectSingle("li:contains(Dodane)").selectSingle("strong").text()
    val description = doc.selectSingle("meta[property=og:description]").attr("content").toOneLineNormalized
    val phone = Option(doc.select("span[data-phone]").toVector).filter(_.nonEmpty).map(_.map(_.attr("data-phone")).mkString(","))
    val title = doc.selectSingle("meta[property=og:title]").attr("content").toOneLineNormalized
    OlxFullOffer(phone, title, description, LocalDate.parse(addDate, dateParser), Some(area))
  }
}