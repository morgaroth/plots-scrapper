package io.morgaroth.parcelsscapper.scrapping.morizon

import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import org.jsoup.Jsoup

import scala.jdk.CollectionConverters._

case class MorizonSearchEntry(id: String, price: Long, link: String)

case class MorizonSearchPager(maxPages: Int)

class MorizonSearchResultsScrapper() extends CommonHtmlHelpers with RichClasses with LazyLogging {
  def getResultsFrom(url: String): Vector[MorizonSearchEntry] = {
    val doc = RJsoup.get(url).get
    parseResultsFrom(doc)
  }

  def parseResultsFrom(str: String): Vector[MorizonSearchEntry] = {
    val doc = Jsoup.parse(str)
    doc.select("div.row--property-list").asScala.toVector
      .filter(_.hasAttr("data-id"))
      .map { entry =>
        val id = entry.attr("data-id").normalize
        val price = entry.selectSingle("meta[itemprop=price]").attr("content").trim.toDouble.toLong
        val link = entry.selectSingle("a.property-url").attr("href")
        MorizonSearchEntry(id, price, link)
      }
  }

  def parsePager(str: String): MorizonSearchPager = {
    val doc = Jsoup.parse(str)
    Option(doc.selectSingle("div.mz-card__item--pagination")).map { pager =>
      val maxPage = pager.select("a").toVector.map(_.text()).filter(_.matches("""\d+""")).map(_.toInt).max
      MorizonSearchPager(maxPage)
    }.getOrElse(MorizonSearchPager(1))
  }

  def parseAllResultsFor(url: String): Vector[MorizonSearchEntry] = {
    assert(!url.contains("page="), "url shall not contain page param")
    val doc = RJsoup.get(url).get
    val pager = parsePager(doc)
    val firstPage = parseResultsFrom(doc)
    (firstPage ++ 2.to(pager.maxPages).flatMap { pageNum =>
      logger.info(s"Checking $pageNum page of morizon")
      getResultsFrom(s"$url&page=$pageNum")
    }).distinctBy(_.id)
  }
}