package io.morgaroth.parcelsscapper.scrapping.otodom

import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import org.jsoup.Jsoup

import scala.jdk.CollectionConverters._


case class OtodomSearchEntry(id: String, trackingId: String, price: Long, areaInMeters: Long, offerer: String, link: String, title: Option[String]) {
  lazy val priceMerMeter = price / areaInMeters
  lazy val isPrivate = offerer == "Oferta prywatna"
}

case class OtodomSearchPager(resultsPerPage: Int, maxPages: Int)

class OtodomSearchResultsScrapper() extends CommonHtmlHelpers with RichClasses with LazyLogging {
  def getResultsFrom(url: String) = {
    val doc = RJsoup.get(url).get
    parseResultsFrom(doc)
  }

  def parseResultsFrom(str: String) = {
    val doc = Jsoup.parse(str)
    doc.getElementsByTag("article").asScala.toVector
      .filter(_.id().startsWith("offer-item-ad"))
      .map { entry =>
        val title = entry.select("span.offer-item-title").text().toOneLineNormalized
        val offerId = entry.attr("data-item-id")
        val trackingId = entry.attr("data-tracking-id")
        val price = validateAndParseMoney(normalizeTextToOneLine(entry.getElementsByClass("offer-item-price").text()))
        val areatext = validateAndParseAreaInSqMeters(normalizeTextToOneLine(entry.getElementsByClass("offer-item-area").text()))
        val link = entry.getElementsByClass("offer-item-header").first().getElementsByAttributeValueStarting("href", "https://www.otodom.pl/oferta").first().attr("href")
        val linkNormalized = if (link.count(_ == '#') == 1) link.split("#").head else link
        val offerer = entry.selectSingle("div.offer-item-details-bottom > ul > li.pull-right").text().toOneLineNormalized
        OtodomSearchEntry(offerId, trackingId, price, areatext, offerer, linkNormalized, Some(title))
      }
  }

  def parsePager(str: String): OtodomSearchPager = {
    val rawDoc = Jsoup.parse(str)
    val element = rawDoc.selectSingle("ul.pager")
    Option(element).map { doc =>
      val maxPages = doc.select("li > a[href^=https]").asScala.map(_.text()).filter(_.nonEmpty).map(_.toInt).max
      val rowsPerPage = rawDoc.select("select[name=nrAdsPerPage] > option[selected=selected]").text().toInt
      OtodomSearchPager(rowsPerPage, maxPages)
    }.getOrElse(OtodomSearchPager(100, 1))
  }

  def parseAllResultsFor(url: String): Vector[OtodomSearchEntry] = {
    assert(!url.contains("page="), "url shall not contain page param")
    val doc = RJsoup.get(url).get
    val firstPage = parseResultsFrom(doc)
    val pager = parsePager(doc)
    (firstPage ++ 2.to(pager.maxPages).flatMap { pageNum =>
      logger.info(s"Checking $pageNum page of otodom")
      getResultsFrom(s"$url&page=$pageNum")
    }).distinctBy(_.id)
  }
}