package io.morgaroth.parcelsscapper.scrapping.gumtree

import io.morgaroth.parcelsscapper.common.{CommonHtmlHelpers, RJsoup, RichClasses}
import io.morgaroth.parcelsscapper.errors.ScrappingException
import io.morgaroth.parcelsscapper.scrapping.DebugHelpers
import org.jsoup.Jsoup

import scala.annotation.tailrec
import scala.jdk.CollectionConverters._
import scala.util.Try

case class GumtreeSearchEntry(id: String, shortId: String, price: Long, link: String)

case class GumtreeSearchPager(maxPages: Int, currentPage: Int, prevPage: Option[String], nextPage: Option[String])

class GumtreeSearchResultsScrapper() extends CommonHtmlHelpers with RichClasses {
  def getResultsFrom(url: String): Vector[GumtreeSearchEntry] = {
    val doc = RJsoup.get(url).get
    parseResultsFrom(doc)
  }

  def parseResultsFrom(str: String): Vector[GumtreeSearchEntry] = {
    val doc = Jsoup.parse(str)
    doc.select("div.tileV1").asScala.toVector
      .map { entry =>
        val reply = entry.selectSingle("div.reply-action")
        val id = reply.selectSingle("div[data-adid]").attr("data-adid")
        val shortId = reply.selectSingle("div[data-short-id]").attr("data-short-id")
        val price = validateAndParseMoney(normalizeTextToOneLine(entry.select("span.ad-price").text()))
        val link = entry.selectSingle("a.href-link").attr("href")
        GumtreeSearchEntry(id, shortId, price, s"https://www.gumtree.pl$link")
      }
  }

  def parsePager(str: String): GumtreeSearchPager = {
    val doc = Jsoup.parse(str)
    try {
      Option(doc.selectSingle("div.desktop-pagination")).map { paginationElem =>
        val lastPageElem = paginationElem.selectSingle("a.pag-box-last")
        val maxPages = Option(lastPageElem).map { lastPageLink =>
          """/page-(\d+)/""".r.findFirstMatchIn(lastPageLink.attr("href")).get.group(1).toInt
        }.getOrElse(paginationElem.select("a.pag-box").toVector.map(_.text()).flatMap(x => Try(x.toInt).toOption).max)
        val allLinksInPager = paginationElem.select("a[href]").asScala.map(_.attr("href")).toSet
        val currentPageNum = paginationElem.selectSingle("*.current-page").text().toInt
        val prevPage = if (currentPageNum > 2) {
          Some(allLinksInPager.find(_.contains(s"/page-${currentPageNum - 1}/")).get)
        } else {
          if (currentPageNum == 2) {
            Some(paginationElem.select("span.prev > a").attr("href"))
          } else None
        }
        val nextPage = if (currentPageNum == maxPages) None else Some(allLinksInPager.find(_.contains(s"/page-${currentPageNum + 1}/")).get)
        GumtreeSearchPager(maxPages, currentPageNum, prevPage.map(x => s"https://www.gumtree.pl$x"), nextPage.map(x => s"https://www.gumtree.pl$x"))
      }.getOrElse {
        GumtreeSearchPager(1, 1, None, None)
      }
    } catch {
      case e: NullPointerException =>
        DebugHelpers.storeDebugDataForParsing(getClass.getSimpleName + ".parsePager", str, "", e)
        throw ScrappingException(str, e)
    }
  }

  def parseAllResultsFor(url: String): Vector[GumtreeSearchEntry] = {
    assert(!url.contains("/page-"), "url shall not contain page param")

    @tailrec def parseAll(link: Option[String], acc: Vector[GumtreeSearchEntry]): Vector[GumtreeSearchEntry] = {
      link match {
        case None => acc
        case Some(l) =>
          val currentHtml = RJsoup.get(l).get
          val thisRows = parseResultsFrom(currentHtml)
          val pager = parsePager(currentHtml)
          parseAll(pager.nextPage, acc ++ thisRows)
      }
    }

    parseAll(Some(url), Vector.empty)
  }
}