name := "parcels-scrapper"

version := "1.0"

scalaVersion := "2.13.1"

scalacOptions ++= Seq("-deprecation", "-Ymacro-annotations")

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "2.1.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "com.github.pathikrit" %% "better-files" % "3.8.0",
  "ch.qos.logback" % "logback-classic" % "1.+",
  "org.jsoup" % "jsoup" % "1.+",
  "org.mongodb.scala" %% "mongo-scala-driver" % "4.0.5",
  "com.typesafe" % "config" % "1.+",
  "org.json4s" %% "json4s-native" % "3.6.8",

  "org.scalatest" %% "scalatest" % "3.1.0" % Test,
)